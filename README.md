# blinking_diode

Simple package for OpenWRT
Board: BeagleBone Black, CPU: TI AM335x

## Getting started

```
git clone https://gitlab.com/kacperlud/blinking_diode
add to openwrt/feeds.conf.default -> src-link blinking_diode /path/to/blinking_diode/repo
cd openwrt
./scripts/feeds update blinking_diode
./scripts/feeds install -a -p blinking_diode
make menuconfig (add package to build and set frequency)
make
```
