include $(TOPDIR)/rules.mk

PKG_NAME:=blinking_diode
PKG_VERSION:=0.1
PKG_RELEASE:=1
PKG_FIXUP:=autoreconf
PKG_INSTALL:=1

PKG_CONFIG_DEPENDS:= \
	CONFIG_BLINK_FREQUENCY_HZ \

define Package/blinking_diode
	SECTION:=utils
	# Select package by default
	#DEFAULT:=y
	CATEGORY:=Utilities
	TITLE:=Blinking diode application.
	MENU:=1
endef

include $(INCLUDE_DIR)/package.mk

define Package/blinking_diode/config
	source "$(SOURCE)/Config.in"
endef

CONFIGURE_ARGS += BLINK_FREQUENCY_HZ=$(CONFIG_BLINK_FREQUENCY_HZ)

define Package/blinking_diode/description
	Blinking diode application.
endef

define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	$(CP) ./src/* $(PKG_BUILD_DIR)/
endef

define Package/blinking_diode/install
	chmod +x ./files/etc/init.d/blinking_diode	
	$(CP) ./files/* $(1)/

	$(INSTALL_DIR) $(1)/usr/bin
	$(INSTALL_BIN) $(PKG_INSTALL_DIR)/usr/bin/blinking_diode $(1)/usr/bin/
endef

$(eval $(call BuildPackage,blinking_diode))