// SPDX-License-Identifier: LGPL-2.1+

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "config.h"

int main(void)
{
	FILE *trigger = fopen("/sys/class/leds/beaglebone:green:usr3/trigger", "w");
	FILE *brightness = fopen("/sys/class/leds/beaglebone:green:usr3/brightness", "w");

	if (trigger == NULL || brightness == NULL) {
		fprintf(stderr, "Cannot open file in /sys/class/leds/beaglebone:green:usr3/\n");
		exit(EXIT_FAILURE);
	}
	int on = 1;

	#ifndef BLINK_FREQUENCY_HZ
		fprintf(stderr, "BLINK_FREQUENCY_HZ not defined!\n");
		exit(EXIT_FAILURE);
	#endif

	if (BLINK_FREQUENCY_HZ <= 0) {
		fprintf(stderr, "BLINK_FREQUENCY_HZ has to be greater than 0\n");
		exit(EXIT_FAILURE);
	}

	fprintf(trigger, "none\n");

	while (1) {
		fprintf(brightness, "%d\n", on);
		fflush(brightness);
		if (!on)
			on = 1;
		else
			on = 0;
		usleep(1000000/BLINK_FREQUENCY_HZ); // 1000000 microseconds = 1 second
	}

	fclose(trigger);
	fclose(brightness);

	return 0;
}
